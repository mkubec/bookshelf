package org.mkubec.bookshelf;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.mkubec.bookshelf.jpa.BookRepository;
import org.mkubec.bookshelf.model.Book;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(SpringRunner.class)
@DataJpaTest
public class BookRepositoryTest {

    @Autowired
    private BookRepository bookRepository;

    @Test
    public void testBookByAuthor() {
        List<Book> foundBooks = bookRepository.findBooksByAuthor("Dan Brown");
        assertThat("Number of books by author", foundBooks.size(), is(5));
    }

    @Test
    public void testAllBooks() {
        List<Book> foundBooks = bookRepository.findAll();
        assertThat("Number of all books", foundBooks.size(), is(19));
    }

}
