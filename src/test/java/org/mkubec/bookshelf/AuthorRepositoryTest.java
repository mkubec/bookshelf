package org.mkubec.bookshelf;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.mkubec.bookshelf.jpa.AuthorRepository;
import org.mkubec.bookshelf.model.Author;
import org.mkubec.bookshelf.model.AuthorResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(SpringRunner.class)
@DataJpaTest
public class AuthorRepositoryTest {

    @Autowired
    private AuthorRepository authorRepository;

    @Test
    public void testMostPopular() {
        List<AuthorResult> mostPopular =  authorRepository.mostPopularAuthors();
        assertThat(mostPopular.get(0).getAuthor().getName(), is("J.K. Rowling"));
        assertThat(mostPopular.get(1).getAuthor().getName(), is("George R.R. Martin"));
        assertThat(mostPopular.get(2).getAuthor().getName(), is("Jo Nesbo"));
    }

    @Test
    public void testAllAuthors() {
        List<Author> allAuthors = authorRepository.findAll();
        assertThat("Find all authors", allAuthors.size(), is(10));
    }

}
