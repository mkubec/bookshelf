package org.mkubec.bookshelf;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.mkubec.bookshelf.jpa.ReaderRepository;
import org.mkubec.bookshelf.model.Reader;
import org.mkubec.bookshelf.model.YearResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ReaderRepositoryTest {

    @Autowired
    private ReaderRepository readerRepository;

    @Test
    public void testReaderByBornYear() {
        List<YearResult> foundReaders = readerRepository.readersByBornYear();
        assertThat(foundReaders.size(), is(5));
        assertThat(foundReaders.get(0).getYear(), is(1999));
        assertThat(foundReaders.get(0).getCount(), is(3L));
    }

    @Test
    public void testAllReaders() {
        List<Reader> foundReaders = readerRepository.findAll();
        assertThat("Number of all readers", foundReaders.size(), is(10));
    }

}
