package org.mkubec.bookshelf.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import java.sql.Date;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class Reader {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    private Date dayOfBirth;

    @ManyToMany
    @JoinTable(name="Reader_Book",
            joinColumns=@JoinColumn(name="reader_id", referencedColumnName="id"),
            inverseJoinColumns=@JoinColumn(name="book_id", referencedColumnName="id"))
    private Set<Book> books = new HashSet<>();

    public Reader() {}

    public Reader(String name, Date dayOfBirth) {
        this.name = name;
        this.dayOfBirth = dayOfBirth;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDayOfBirth() {
        return dayOfBirth;
    }

    public void setDayOfBirth(Date dayOfBirth) {
        this.dayOfBirth = dayOfBirth;
    }

    public Set<Book> getBooks() {
        return books;
    }

    public void setBooks(Set<Book> books) {
        this.books = books;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reader reader = (Reader) o;
        return Objects.equals(name, reader.name) &&
                Objects.equals(dayOfBirth, reader.dayOfBirth);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, dayOfBirth);
    }

    @Override
    public String toString() {
        return "Reader{" +
                "name='" + name + '\'' +
                ", dayOfBirth=" + dayOfBirth +
                '}';
    }

}
