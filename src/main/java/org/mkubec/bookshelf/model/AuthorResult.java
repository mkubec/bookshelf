package org.mkubec.bookshelf.model;

public class AuthorResult {

    private Author author;
    private long count;

    public AuthorResult(Author author, long count) {
        this.author = author;
        this.count = count;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

}
