package org.mkubec.bookshelf.model;

import java.io.Serializable;

public class YearResult implements Serializable {

    private int year;
    private long count;

    public YearResult(int year, long count) {
        this.year = year;
        this.count = count;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

}
