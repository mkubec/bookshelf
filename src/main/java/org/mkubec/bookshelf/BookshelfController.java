package org.mkubec.bookshelf;

import org.mkubec.bookshelf.jpa.AuthorRepository;
import org.mkubec.bookshelf.jpa.BookRepository;
import org.mkubec.bookshelf.jpa.ReaderRepository;

import org.mkubec.bookshelf.model.Author;
import org.mkubec.bookshelf.model.AuthorResult;
import org.mkubec.bookshelf.model.Book;
import org.mkubec.bookshelf.model.YearResult;
import org.mkubec.bookshelf.model.Reader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@EnableAutoConfiguration
@RequestMapping(path = "/bookshelf")
public class BookshelfController {

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private ReaderRepository readerRepository;

    // 3.a. Listing all books for given author
    @RequestMapping(value = "/books-by-author", method = GET)
    public List<Book> listBooksByAuthor(@RequestParam(value = "name") String name) {
        return bookRepository.findBooksByAuthor(name);
    }

    // 3.b. Adding new Authors
    @RequestMapping(value = "/add-author", method = POST)
    public Author addAuthor(@RequestBody Author author) {
        return authorRepository.save(author);
    }

    // 3.c. Showing number of readers born in each year
    @RequestMapping(value = "/readers-count-by-born-year", method = GET)
    public List<YearResult> getNumbersOfReadersBornInYear() {
        return readerRepository.readersByBornYear();
    }

    // 3.d. Listing 3 the most popular authors with their popularity
    @RequestMapping(value = "/most-popular-authors", method = GET)
    public List<AuthorResult> getMostPopularAuthors(@RequestParam(value = "limit", defaultValue = "3") int limit) {
        return authorRepository.mostPopularAuthors().subList(0, limit);
    }

    // Test resource
    @RequestMapping(value = "/authors", method = GET)
    public List<Author> getAuthors() {
        return authorRepository.findAll();
    }

    // Test resource
    @RequestMapping(value = "/books", method = GET)
    public List<Book> getBooks() {
        return bookRepository.findAll();
    }

    // Test resource
    @RequestMapping(value = "/book", method = GET)
    public Book getBooks(@RequestParam(value = "id") int id) {
        Optional<Book> optionalBook = bookRepository.findById(id);
        return optionalBook.orElse(null);
    }

    // Test resource
    @RequestMapping(value = "/readers", method = GET)
    public List<Reader> getReaders() {
        return readerRepository.findAll();
    }

}
