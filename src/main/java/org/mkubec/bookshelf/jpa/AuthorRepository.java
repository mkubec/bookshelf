package org.mkubec.bookshelf.jpa;

import org.mkubec.bookshelf.model.Author;
import org.mkubec.bookshelf.model.AuthorResult;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AuthorRepository extends JpaRepository<Author,Integer> {

    // Listing 3 the most popular authors with their popularity
    // (popularity as a total number of his/her books read by Readers)

    @Query("select new org.mkubec.bookshelf.model.AuthorResult(a, count(a)) " +
            "from Reader r join r.books b join b.authors a group by a order by count(a) desc")
    List<AuthorResult> mostPopularAuthors();

}
