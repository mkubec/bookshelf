package org.mkubec.bookshelf.jpa;

import org.mkubec.bookshelf.model.YearResult;
import org.mkubec.bookshelf.model.Reader;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ReaderRepository extends JpaRepository<Reader, Integer> {

    // Showing number of readers born in each year

    @Query("select new org.mkubec.bookshelf.model.YearResult(EXTRACT(YEAR FROM r.dayOfBirth), " +
            "count(r.name)) from Reader r group by EXTRACT(YEAR FROM r.dayOfBirth) order by count(r.name) desc")
    List<YearResult> readersByBornYear();

}
