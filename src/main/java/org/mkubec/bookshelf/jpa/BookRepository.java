package org.mkubec.bookshelf.jpa;

import org.mkubec.bookshelf.model.Book;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BookRepository extends JpaRepository<Book, Integer> {

    // Listing all books for given author

    @Query("select b from Book b join b.authors auth where auth.name = :authorName")
    List<Book> findBooksByAuthor(String authorName);

}
