INSERT into Author (name) VALUES ('Jo Nesbo');
INSERT into Author (name) VALUES ('J.K. Rowling');
INSERT into Author (name) VALUES ('Margaret Atwood');
INSERT into Author (name) VALUES ('James Comey');
INSERT into Author (name) VALUES ('John Sandford');
INSERT into Author (name) VALUES ('Philip Pullman');
INSERT into Author (name) VALUES ('Jamie Oliver');
INSERT into Author (name) VALUES ('Dan Brown');
INSERT into Author (name) VALUES ('Rick Riordan');
INSERT into Author (name) VALUES ('George R.R. Martin');
MERGE INTO Book USING (VALUES 'Harry Potter 1') Temp (name) ON (Book.name = Temp.name)
  WHEN NOT MATCHED THEN INSERT (name) VALUES (Temp.name);
INSERT into Book_Author (book_id, author_id) VALUES (
  (select id from Book where name = 'Harry Potter 1'),
  (select id from Author where name = 'J.K. Rowling')
);
MERGE INTO Book USING (VALUES 'Harry Potter 2') Temp (name) ON (Book.name = Temp.name)
  WHEN NOT MATCHED THEN INSERT (name) VALUES (Temp.name);
INSERT into Book_Author (book_id, author_id) VALUES (
  (select id from Book where name = 'Harry Potter 2'),
  (select id from Author where name = 'J.K. Rowling')
);
MERGE INTO Book USING (VALUES 'Harry Potter 3') Temp (name) ON (Book.name = Temp.name)
  WHEN NOT MATCHED THEN INSERT (name) VALUES (Temp.name);
INSERT into Book_Author (book_id, author_id) VALUES (
  (select id from Book where name = 'Harry Potter 3'),
  (select id from Author where name = 'J.K. Rowling')
);
MERGE INTO Book USING (VALUES 'Harry Potter 4') Temp (name) ON (Book.name = Temp.name)
  WHEN NOT MATCHED THEN INSERT (name) VALUES (Temp.name);
INSERT into Book_Author (book_id, author_id) VALUES (
  (select id from Book where name = 'Harry Potter 4'),
  (select id from Author where name = 'J.K. Rowling')
);
MERGE INTO Book USING (VALUES 'Harry Potter 5') Temp (name) ON (Book.name = Temp.name)
  WHEN NOT MATCHED THEN INSERT (name) VALUES (Temp.name);
INSERT into Book_Author (book_id, author_id) VALUES (
  (select id from Book where name = 'Harry Potter 5'),
  (select id from Author where name = 'J.K. Rowling')
);
MERGE INTO Book USING (VALUES 'Harry Potter 6') Temp (name) ON (Book.name = Temp.name)
  WHEN NOT MATCHED THEN INSERT (name) VALUES (Temp.name);
INSERT into Book_Author (book_id, author_id) VALUES (
  (select id from Book where name = 'Harry Potter 6'),
  (select id from Author where name = 'J.K. Rowling')
);
MERGE INTO Book USING (VALUES 'Harry Potter 7') Temp (name) ON (Book.name = Temp.name)
  WHEN NOT MATCHED THEN INSERT (name) VALUES (Temp.name);
INSERT into Book_Author (book_id, author_id) VALUES (
  (select id from Book where name = 'Harry Potter 7'),
  (select id from Author where name = 'J.K. Rowling')
);
MERGE INTO Book USING (VALUES 'Harry Potter 7') Temp (name) ON (Book.name = Temp.name)
  WHEN NOT MATCHED THEN INSERT (name) VALUES (Temp.name);
INSERT into Book_Author (book_id, author_id) VALUES (
  (select id from Book where name = 'Harry Potter 7'),
  (select id from Author where name = 'George R.R. Martin')
);
MERGE INTO Book USING (VALUES 'Harry Potter 7') Temp (name) ON (Book.name = Temp.name)
  WHEN NOT MATCHED THEN INSERT (name) VALUES (Temp.name);
INSERT into Book_Author (book_id, author_id) VALUES (
  (select id from Book where name = 'Harry Potter 7'),
  (select id from Author where name = 'Jo Nesbo')
);
MERGE INTO Book USING (VALUES 'Game of Thrones') Temp (name) ON (Book.name = Temp.name)
  WHEN NOT MATCHED THEN INSERT (name) VALUES (Temp.name);
INSERT into Book_Author (book_id, author_id) VALUES (
  (select id from Book where name = 'Game of Thrones'),
  (select id from Author where name = 'George R.R. Martin')
);
MERGE INTO Book USING (VALUES 'A Song of Ice and Fire') Temp (name) ON (Book.name = Temp.name)
  WHEN NOT MATCHED THEN INSERT (name) VALUES (Temp.name);
INSERT into Book_Author (book_id, author_id) VALUES (
  (select id from Book where name = 'A Song of Ice and Fire'),
  (select id from Author where name = 'George R.R. Martin')
);
MERGE INTO Book USING (VALUES 'The Winds of Winter') Temp (name) ON (Book.name = Temp.name)
  WHEN NOT MATCHED THEN INSERT (name) VALUES (Temp.name);
INSERT into Book_Author (book_id, author_id) VALUES (
  (select id from Book where name = 'The Winds of Winter'),
  (select id from Author where name = 'George R.R. Martin')
);
MERGE INTO Book USING (VALUES 'The Bat') Temp (name) ON (Book.name = Temp.name)
  WHEN NOT MATCHED THEN INSERT (name) VALUES (Temp.name);
INSERT into Book_Author (book_id, author_id) VALUES (
  (select id from Book where name = 'The Bat'),
  (select id from Author where name = 'Jo Nesbo')
);
MERGE INTO Book USING (VALUES 'Cockroaches') Temp (name) ON (Book.name = Temp.name)
  WHEN NOT MATCHED THEN INSERT (name) VALUES (Temp.name);
INSERT into Book_Author (book_id, author_id) VALUES (
  (select id from Book where name = 'Cockroaches'),
  (select id from Author where name = 'Jo Nesbo')
);
MERGE INTO Book USING (VALUES 'The Redbreast') Temp (name) ON (Book.name = Temp.name)
  WHEN NOT MATCHED THEN INSERT (name) VALUES (Temp.name);
INSERT into Book_Author (book_id, author_id) VALUES (
  (select id from Book where name = 'The Redbreast'),
  (select id from Author where name = 'Jo Nesbo')
);
MERGE INTO Book USING (VALUES 'Nemesis') Temp (name) ON (Book.name = Temp.name)
  WHEN NOT MATCHED THEN INSERT (name) VALUES (Temp.name);
INSERT into Book_Author (book_id, author_id) VALUES (
  (select id from Book where name = 'Nemesis'),
  (select id from Author where name = 'Jo Nesbo')
);
MERGE INTO Book USING (VALUES 'The Da Vinci Code') Temp (name) ON (Book.name = Temp.name)
  WHEN NOT MATCHED THEN INSERT (name) VALUES (Temp.name);
INSERT into Book_Author (book_id, author_id) VALUES (
  (select id from Book where name = 'The Da Vinci Code'),
  (select id from Author where name = 'Dan Brown')
);
MERGE INTO Book USING (VALUES 'Inferno') Temp (name) ON (Book.name = Temp.name)
  WHEN NOT MATCHED THEN INSERT (name) VALUES (Temp.name);
INSERT into Book_Author (book_id, author_id) VALUES (
  (select id from Book where name = 'Inferno'),
  (select id from Author where name = 'Dan Brown')
);
MERGE INTO Book USING (VALUES 'The Lost Symbol') Temp (name) ON (Book.name = Temp.name)
  WHEN NOT MATCHED THEN INSERT (name) VALUES (Temp.name);
INSERT into Book_Author (book_id, author_id) VALUES (
  (select id from Book where name = 'The Lost Symbol'),
  (select id from Author where name = 'Dan Brown')
);
MERGE INTO Book USING (VALUES 'Digital Fortress') Temp (name) ON (Book.name = Temp.name)
  WHEN NOT MATCHED THEN INSERT (name) VALUES (Temp.name);
INSERT into Book_Author (book_id, author_id) VALUES (
  (select id from Book where name = 'Digital Fortress'),
  (select id from Author where name = 'Dan Brown')
);
MERGE INTO Book USING (VALUES 'Origin') Temp (name) ON (Book.name = Temp.name)
  WHEN NOT MATCHED THEN INSERT (name) VALUES (Temp.name);
INSERT into Book_Author (book_id, author_id) VALUES (
  (select id from Book where name = 'Origin'),
  (select id from Author where name = 'Dan Brown')
);
INSERT into Reader (name, day_of_birth) VALUES ('John Doe', '1999-03-25');
INSERT into Reader (name, day_of_birth) VALUES ('Jane Snow', '1999-12-01');
INSERT into Reader (name, day_of_birth) VALUES ('Jack Poole', '1980-07-12');
INSERT into Reader (name, day_of_birth) VALUES ('Jesse Ford', '1975-08-08');
INSERT into Reader (name, day_of_birth) VALUES ('Peter Pan', '1995-09-15');
INSERT into Reader (name, day_of_birth) VALUES ('Fiona Shrek', '1999-02-20');
INSERT into Reader (name, day_of_birth) VALUES ('Ann Bird', '1995-01-21');
INSERT into Reader (name, day_of_birth) VALUES ('Thomas Ambrose', '1998-04-02');
INSERT into Reader (name, day_of_birth) VALUES ('Richard Beckham', '1998-06-04');
INSERT into Reader (name, day_of_birth) VALUES ('Margaret Green', '1998-11-13');
INSERT into Reader_Book (reader_id, book_id) VALUES (
  (select id from Reader where name = 'John Doe'),
  (select id from Book where name = 'Harry Potter 1')
);
INSERT into Reader_Book (reader_id, book_id) VALUES (
  (select id from Reader where name = 'John Doe'),
  (select id from Book where name = 'Harry Potter 2')
);
INSERT into Reader_Book (reader_id, book_id) VALUES (
  (select id from Reader where name = 'John Doe'),
  (select id from Book where name = 'Harry Potter 3')
);
INSERT into Reader_Book (reader_id, book_id) VALUES (
  (select id from Reader where name = 'John Doe'),
  (select id from Book where name = 'Harry Potter 4')
);
INSERT into Reader_Book (reader_id, book_id) VALUES (
  (select id from Reader where name = 'John Doe'),
  (select id from Book where name = 'Harry Potter 5')
);
INSERT into Reader_Book (reader_id, book_id) VALUES (
  (select id from Reader where name = 'John Doe'),
  (select id from Book where name = 'Harry Potter 6')
);
INSERT into Reader_Book (reader_id, book_id) VALUES (
  (select id from Reader where name = 'John Doe'),
  (select id from Book where name = 'Harry Potter 7')
);
INSERT into Reader_Book (reader_id, book_id) VALUES (
  (select id from Reader where name = 'Jane Snow'),
  (select id from Book where name = 'Game of Thrones')
);
INSERT into Reader_Book (reader_id, book_id) VALUES (
  (select id from Reader where name = 'Jane Snow'),
  (select id from Book where name = 'A Song of Ice and Fire')
);
INSERT into Reader_Book (reader_id, book_id) VALUES (
  (select id from Reader where name = 'Jane Snow'),
  (select id from Book where name = 'The Winds of Winter')
);
INSERT into Reader_Book (reader_id, book_id) VALUES (
  (select id from Reader where name = 'Jack Poole'),
  (select id from Book where name = 'Nemesis')
);
INSERT into Reader_Book (reader_id, book_id) VALUES (
  (select id from Reader where name = 'Thomas Ambrose'),
  (select id from Book where name = 'Inferno')
);
INSERT into Reader_Book (reader_id, book_id) VALUES (
  (select id from Reader where name = 'Thomas Ambrose'),
  (select id from Book where name = 'Origin')
);
