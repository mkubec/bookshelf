CREATE TABLE Author (
  id INTEGER IDENTITY PRIMARY KEY,
  name VARCHAR(100) UNIQUE NOT NULL
);

CREATE TABLE Book (
  id INTEGER IDENTITY PRIMARY KEY,
  name VARCHAR(100) UNIQUE NOT NULL
);

CREATE TABLE Reader (
  id INTEGER IDENTITY PRIMARY KEY,
  name VARCHAR(100) UNIQUE NOT NULL,
  day_of_birth DATE NOT NULL
);

CREATE TABLE Book_Author (
  book_id INTEGER NOT NULL,
  author_id INTEGER NOT NULL,
  FOREIGN KEY (book_id) REFERENCES Book(id),
  FOREIGN KEY (author_id) REFERENCES Author(id),
  PRIMARY KEY (book_id, author_id)
);

CREATE TABLE Reader_Book (
  reader_id INTEGER NOT NULL,
  book_id INTEGER NOT NULL,
  FOREIGN KEY (reader_id) REFERENCES Reader(id),
  FOREIGN KEY (book_id) REFERENCES Book(id),
  PRIMARY KEY (reader_id, book_id)
);
