# bookshelf

Simple Bookshelf Application based on Spring Boot with HSQLDB embedded database

Building

    mvn clean package

Running

    mvn spring-boot:run

Rest Endpoints

1. Listing all books for given author
    
    GET localhost:8080/bookshelf/books-by-author?name=J.K. Rowling

1. Adding new Authors

    POST localhost:8080/bookshelf/add-author
    Body: {"name": "Bozena Nemcova"}
        
1. Showing number of readers born in each year

    GET localhost:8080/bookshelf/readers-count-by-born-year

1. Listing 3 the most popular authors with their popularity 

    GET localhost:8080/bookshelf/most-popular-authors
