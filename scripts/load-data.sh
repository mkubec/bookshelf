#!/usr/bin/env bash

OLDIFS=$IFS
IFS=,

while read name
do
    echo "INSERT into Author (name) VALUES ('$name');"
done < ../data/authors.csv

while read book author
do
    echo "MERGE INTO Book USING (VALUES '$book') Temp (name) ON (Book.name = Temp.name)
  WHEN NOT MATCHED THEN INSERT (name) VALUES (Temp.name);"
    echo "INSERT into Book_Author (book_id, author_id) VALUES (
  (select id from Book where name = '$book'),
  (select id from Author where name = '$author')
);"
done < ../data/books.csv

while read name date
do
    echo "INSERT into Reader (name, day_of_birth) VALUES ('$name', '$date');"
done < ../data/readers.csv

while read reader book
do
    echo "INSERT into Reader_Book (reader_id, book_id) VALUES (
  (select id from Reader where name = '$reader'),
  (select id from Book where name = '$book')
);"
done < ../data/readers-books.csv

IFS=$OLDIFS
